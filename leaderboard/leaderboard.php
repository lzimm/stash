<?php

// this leaderboard implementation uses twig to render templates
// as well as React (http://reactphp.org/) as the base server
// infrastructure, which allows us to do simple real time updates
// without having to continuously poll against the server (and
// can have browsers simple have single outstanding long poll
// requests left open until another user makes an update)
//
// furthermore, in order to keep things as simple as possible
// a database is not used here and instead we're storing all
// data in volatile memory (though descriptions of how in memory)
// operations would be performed against an actual database
// are provided)

require 'vendor/autoload.php';

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);
$http = new React\Http\Server($socket, $loop);

$loader = new Twig_Loader_Filesystem(dirname(__FILE__));
$twig = new Twig_Environment($loader);

// we use this global listener array to maintain a list of
// long poll requests waiting for the next update

$listeners = array();

// to keep things simple, we're just storing everything in memory
// rather than against an actual DB, though see the notes below 
// for documentation on how operations against both $updates and 
// $scores would look if we were in fact against a real database

$update = 0;

$scores = array(
    '1' => array('id' => '1', 'name' => "Nikola Tesla", 'score' => 0),
    '2' => array('id' => '2', 'name' => "Grace Hopper", 'score' => 0),
    '3' => array('id' => '3', 'name' => "Marie Curie", 'score' => 0),
    '4' => array('id' => '4', 'name' => "Carl Friedrich Gauss", 'score' => 0),
    '5' => array('id' => '5', 'name' => "Ada Lovelace", 'score' => 0),
    '6' => array('id' => '6', 'name' => "Clause Shannon", 'score' => 0)
);

// as described below, this ranking function basically decomposes down
// to something very similar to the following (as a query) if it were 
// against an actual (mysql) database rather than just working against
// an in memory list:
//
// "SELECT * FROM `leaderboard` ORDER BY `score` DESC, `name` DESC;"
//
// note: in this implemenation we're sorting a duplicate (ie: pass by val)
// of the $scores array (the $rankable param here) so that we don't destruct
// over the global above

function ranking($rankable) {
    uksort($rankable, function($a, $b) use ($rankable) {
        $a = $rankable[$a];
        $b = $rankable[$b];
        return ($a['score'] > $b['score']) 
                    ? -1 
                    : (($a['score'] < $b['score'])
                        ? 1
                        : strcmp($a['name'], $b['name']));
    });

    foreach ($rankable as &$value) {
        $value['score'] = number_format($value['score']);
    }

    return $rankable;
};

// base handler to generate a basic html leaderboard listing, inlining all
// of the css and javascript directly in the page, primarily for simplicity
// and so that this entire thing can run easilly against the built in webserver
// provided by React

$indexHandler = function($request, $response) use (&$twig, &$scores, &$update) {
    $ranked = ranking($scores);

    $rendered = $twig->render('index.html', array('leaderboard' => $ranked, 'update' => $update));
    $response->writeHead(200, array('Content-Type' => 'text/html'));
    $response->end($rendered);
};

// handler to set aside long polling requests that the browser will make in order
// to listen for updates made by other users (the js side of this generates a random string
// appended to the request path so that the browser doesn't block multiple outstanding
// requests to the same url, as was happening for me in chrome)

$listenHandler = function($request, $response) use (&$listeners, &$scores, &$update) {
    $queryParams = $request->getQuery();
    $offset = isset($queryParams['offset']) ? $queryParams['offset'] : -1;

    // we use simple long polling in order to implement make shift real time
    // updates, with a special cursor parameter (offset) which we use just to make
    // sure that listeners don't fall into the condition where they may miss updates
    // (where a second vote occurs during the period where they were reconnecting
    // their longpoll)

    if ($offset < $update) {
        $ranked = ranking($scores);
        $response->writeHead(200, array('Content-Type' => 'text/json'));
        $response->end(json_encode(array('items' => $ranked, 'update' => $update)));
    } else {
        array_push($listeners, $response);
    }
};

// simple handler that lets users vote and sends very simple updates to all
// the listening requests -- noting that same of those requests may have timed
// out a long time ago, though since React doesn't seem to provide a way for
// us to track that, the $listeners array above may grow unbounded between updates
// (even though its completely cleared out every time a vote does occur)

$voteHandler = function($request, $response) use (&$listeners, &$scores, &$update) {
    $matched = array();
    if (!preg_match('/^\/vote\/(\d+)\/?$/', $request->getPath(), $matched)) {
        $response->writeHead(200, array('Content-Type' => 'text/json'));
        $response->end(json_encode(array("error" => "true")));
        return;
    }

    $id = $matched[1];
    if (!isset($scores[$id])) {
        $response->writeHead(200, array('Content-Type' => 'text/json'));
        $response->end(json_encode(array("error" => "invalid id")));
        return;
    }

    // As opposed to just storing this in memory, if we were going
    // to DB here (mysql), the following, as a sql query, would look
    // more along the lines of:
    //
    // "INSERT INTO `updates` (`vote_for_id`) VALUES ('{ id }'');"
    // where { id } is the properly escaped matched leaderboard id
    //
    // then to track the update number (which we use to make sure that
    // listeners don't accidently miss updates during the course of reconnecting
    // their polling requests), and assuming there's an autoincrement
    // id field on the `updates` table, in the most simple implementation
    // of this entire thing, we'd just select the last inserted id, even
    // though that presents scalability, where if we need to achieve really
    // high performance we'd need some sort of service that can generate
    // monotonically increasing ids for us to track update numbers with

    $update++;

    // Aligning with the above, if we were storing in DB, and not just in
    // memory, then the following, again as a sql query, would look more 
    // along the lines of:
    //
    // "UPDATE `leaderboard` SET `score` = `score` + 5 WHERE `id` = '{ id }';"
    //
    // and
    //
    // "SELECT * FROM `leaderboard` ORDER BY `score` DESC, `name` DESC;"
    //
    // respectively

    $scores[$id]['score'] += 5;
    $ranked = array_values(ranking($scores));

    $queue = $listeners;
    $listeners = array();

    $json = json_encode(array('items' => $ranked, 'update' => $update));

    foreach ($queue as $listener) {
        $listener->writeHead(200, array('Content-Type' => 'text/json'));
        $listener->end($json);
    }

    $response->writeHead(200, array('Content-Type' => 'text/json'));
    $response->end($json);
};

// setting all the handlers above to work against both GET and POST
// requests by matching against a very primitive routing system

$handlers = array(
    '/^(GET|POST) \/$/'                 => $indexHandler,
    '/^(GET|POST) \/listen\/(.*)\/?$/'  => $listenHandler,
    '/^(GET|POST) \/vote\/(\d+)\/?$/'   => $voteHandler
);

// setting up the base handler itself that we can feed directly into
// React's main event loop

$app = function ($request, $response) use ($handlers) {
    $matchable = sprintf("%s %s", $request->getMethod(), $request->getPath());
    foreach ($handlers as $pattern => $handler) {
        if (preg_match($pattern, $matchable)) {
            $handler($request, $response);
            return;
        }
    }

    $response->writeHead(404, array('Content-Type' => 'text/html'));
    $response->end("invalid path :(");
};

// and finally, setting up React itself and just running
// against its built in http server, noting also, that since
// the above data is stored in memory, if this script crashes
// then running a daemonized process to restart it will solve
// the issue of the server going down during an exception,
// but not the persistence issue as all updates will have been
// lost

$http->on('request', $app);
$socket->listen(1337);

echo "Server running at http://127.0.0.1:1337\n";

$loop->run("so");

?>
